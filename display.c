
#include "display.h"
#include "tools.h"

#include <string.h>
#include <stdlib.h>

screen_t screen;
void dis_vprint(vector *msg)
{
	// init display
	lcd_clear(&screen);
	lcd_home(&screen);

	// print stuff
	uint8_t *text = "", i;
	for (i = 0; i < vector_count(msg)-1; i++)
	{
		text = strconcat(text, vector_get(msg, i));
		text = strconcat(text, " ");
	}

	lcd_print(&screen, text);
	lcd_render(&screen);

	free(text);
}

void dis_print(uint8_t *msg)
{
	// init display
	lcd_clear(&screen);
	lcd_home(&screen);

	// print stuff
	lcd_print(&screen, msg);
	lcd_render(&screen);
}
