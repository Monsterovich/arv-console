#
# $Id$
#

#.DEFAULT:
#.SUFFIXES: .c .o .hex .elf
#.PHONY:
.SECONDARY:

all: main.hex

ARCH = ATMEGA328P
DEVICE = /dev/ttyUSB0
SRC = main.o fifo.o tools.o console.o i2clcd.o twim.o mpu6050.o display.o vector.o

CFLAGS= -I. -std=c99 -DF_CPU=16000000UL -Os -mmcu=atmega328p
LDFLAGS= -s -Os -mmcu=atmega328p

main.elf: $(SRC)
	avr-gcc $(LDFLAGS) -o $@ $(SRC)

%.o: %.c
	avr-gcc $(CFLAGS) -c -o $@ $<

%.elf: %.o
	avr-gcc $(LDFLAGS) -o $@ $<

%.hex: %.elf
	avr-objcopy -O ihex -R .eeprom $< $@

upload: main.hex
	avrdude -qq -c arduino -p $(ARCH) -P $(DEVICE) -b 115200 -U flash:w:main.hex

backup:
	avrdude -F -V -c arduino -p $(ARCH) -P $(DEVICE) -b 115200 -U flash:r:backup.hex:i

run:
	avrdude -qq -c arduino -p $(ARCH) -P $(DEVICE) -b 115200 -U flash:w:main.hex
	sudo chmod 666 $(DEVICE) && cu -s 38400 -l $(DEVICE)


clean:
	rm -f *.i *.o *.elf *~ main.hex 

#EOF
