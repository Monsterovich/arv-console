
#include <stdlib.h>
#include <stdio.h>

#include "console.h"
#include "fifo.h"
#include "tools.h"
#include "display.h"
#include "vector.h"

#define DELIM " "
#define TOKEN_SIZE 32
#define TOKEN_LIMIT 16

COMMAND(hello)
{
	printf("Hello there!\n\r");
}

COMMAND(die)
{
	exit(0);
}

COMMAND(display)
{
	dis_vprint(args);
}

uint16_t adc_read(uint8_t ch) {

	if (ch > 7)
		return 0;

	ADMUX = (ADMUX & 0xF0) | ch;		/* Channel selection */
	ADCSRA |= (1 << ADSC);			  /* Start conversion */

	while (!ADCSRA & (1 << ADIF));
	ADCSRA |= (1 << ADIF);

	return (ADC);
}

COMMAND(test)
{
	while (1) {
		if (getchar() == 'c')
			return NULL;

		uint8_t str[32];
		sprintf(str, "Rotor: %d", adc_read(0));
		dis_print(str);
		delay(25);
	}
}


Commands commands[] =
{
	{ "hello", &hello },
	{ "die", &die },
	{ "display", &display },
	{ "test", &test }
};

void parsecmd(uint8_t *cmd)
{
	if (!strcmp(cmd, ""))
		return;

	uint8_t *token;
	uint8_t keyword[TOKEN_SIZE];

	vector args;
	vector_init(&args);

	/* get the first token */
	token = strtok(cmd, DELIM);
	strcpy(keyword, token);

	// parse other tokens
	while (token != NULL) {
		token = strtok (NULL, DELIM);
		vector_add(&args, token);
	}

	uint8_t i;
	for (i = 0; i < (sizeof commands / sizeof *commands); ++i)
	{
		if (!strcmp(commands[i].name, keyword))
		{
			(void)commands[i].cmd(keyword, &args);
			vector_free(&args);
			free(token);
			return;
		}
	}
	printf("%s: command not found.\n\r", keyword);
}
