/* $Id$ */

#ifndef TOOLS_H_IRT
#define TOOLS_H_IRT

#define MAX_LINE_LEN 1024


#define delay(x) _delay_ms(x)

#include <stdbool.h>

char* strconcat(const char *s1, const char *s2);

#endif
/* EOF */
