
#include <string.h>
#include <stdint.h>

#include "vector.h"
 
typedef void *(*Command)( uint8_t *name, vector *args);
#define COMMAND(signal) void *signal (uint8_t *name, vector *args)
typedef struct _Commands 
{ 
	uint8_t *name ; 
	Command cmd; 
} Commands;

void parsecmd(uint8_t *cmd);
